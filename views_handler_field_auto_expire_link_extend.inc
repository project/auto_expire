<?php

/**
 * Field handler to present an extend node link.
 */
class views_handler_field_auto_expire_link_extend extends views_handler_field_node_link {
  function construct() {
    parent::construct();
  }

  function render($values) {
  
    $nid = $values->{$this->aliases['nid']};
    
    // check if user has rights to extend and if the node is extendable right now
    if (_auto_expire_can_user_extend($nid, TRUE)) {
      $text = !empty($this->options['text']) ? $this->options['text'] : t('extend');
      return l($text, "node/$nid/expiry", array('query' => drupal_get_destination()));
    }
    return;

  }
}